# Question 1
data(iris)
iris_quant <- iris[-c(5)]
pc_iris <- princomp(iris_quant)
biplot(pc_iris)
raw <- pc_iris$scores
plot(raw[,1], raw[,2], xlab = "Comp1", ylab="Comp2")


plot(raw[,1], raw[,2], xlab = "Comp1", ylab="Comp2", col=iris$Species, main="Représentation des variables avec coloration par espèce")
legend('bottomright', legend = levels(iris$Species), col = 1:3, cex = 1, pch = 1)

# Détermination du nombnre optimal de clusters
wss <- (nrow(iris_quant)-1)*sum(apply(iris_quant,2,var))
for (i in 2:15) 
  wss[i] <- sum(kmeans(iris_quant, centers=i)$withinss)
plot(1:15, wss, type="b", xlab="Number of Clusters", ylab="Within groups sum of squares") 

library(cluster)
# Affichage du graphe avec les clusters encerclés
fit <- kmeans(iris_quant, 2)
clusplot(iris_quant, fit$cluster, color=TRUE, shade=FALSE,labels=1, lines=0)


# Question 2

crabs2 <- read.csv("crabs2.csv", sep=',')
crabs_quant <- crabs2[-c(5,6)]

pc_crabs <- princomp(crabs_quant)
biplot(pc_crabs)
raw <- pc_crabs$scores
plot(raw[,1], raw[,2], xlab = "Comp1", ylab="Comp2")

# Coloration par sexe
plot(raw[,1], raw[,2], xlab = "Comp1", ylab="Comp2", col=crabs2$sex, main="Représentation des variables avec coloration par sexe")
legend('bottomright', legend = levels(crabs2$sex), col = 1:2, cex = 1, pch = 1)

# Coloration par espèce
plot(raw[,1], raw[,2], xlab = "Comp1", ylab="Comp2", col=crabs2$sp, main="Représentation des variables avec coloration par expèce")
legend('bottomright', legend = levels(crabs2$sp), col = 1:3, cex = 1, pch = 1)  



# Question 3
mut <- read.csv("mutations2.csv", header=T, row.names=1)
mut <- as.dist(mut, diag=T, upper=T)

library(MASS)

# Calcule la qualité d'une représentation sur k dimensions
qualite <- function(eig_val, k){
  sum(eig_val[1:k])/ sum(eig_val) *100
}

k <- 2
AFTD <- cmdscale(mut, eig=TRUE, k)
plot(Shepard(mut, AFTD$points), xlab="Proximités", ylab="Distances", main="Diagramme de Sphepard des données crabs2 (k=2)")
abline(a=0,b=1,col="blue")
qualite(AFTD$eig,k)

k <- 3
AFTD <- cmdscale(mut, eig=TRUE, k)
plot(Shepard(mut, AFTD$points), xlab="Proximités", ylab="Distances", main="Diagramme de Sphepard des données crabs2 (k=3)")
abline(a=0,b=1,col="blue")
qualite(AFTD$eig,k)

k <- 4
AFTD <- cmdscale(mut, eig=TRUE, k)
plot(Shepard(mut, AFTD$points), xlab="Proximités", ylab="Distances", main="Diagramme de Sphepard des données crabs2 (k=4)")
abline(a=0,b=1,col="blue")
qualite(AFTD$eig,k)

k <- 5
AFTD <- cmdscale(mut, eig=TRUE, k)
plot(Shepard(mut, AFTD$points), xlab="Proximités", ylab="Distances", main="Diagramme de Sphepard des données crabs2 (k=5)")
abline(a=0,b=1,col="blue")
qualite(AFTD$eig,k)


# Représentation de l'AFTD dans le premier plan factoriel
AFTD <- cmdscale(mut, eig=TRUE, 2)
plot(AFTD$points[,1], AFTD$points[,2], xlab="Comp1", ylab="Comp2", main = "Représentation des individus dans le premier plan factoriel")
text(x = AFTD$points[,1], y = AFTD$points[,2], labels=labels(mut)) 
