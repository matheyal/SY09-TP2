iter <- function(tab, data){
	tabRes<-matrix(1,9,1);
	for(k in 2:10){
		min<-1000;
		for(i in 1:100){
			kmeansIris <- kmeans(data, k);
			tab[k-1,i] <- kmeansIris$tot.withinss;
			if(tab[k-1,i]<min){
				min <-tab[k-1,i];
			}
		}
		tabRes[k-1,1]<-min;
	}
	tabRes;
	
}
