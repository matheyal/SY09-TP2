\documentclass{article}
\usepackage[francais]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{graphicx}
\usepackage{bbm}
\usepackage{subfig}
\usepackage[hmarginratio=1:1,top=32mm,columnsep=20pt]{geometry}
\usepackage{multirow}
\usepackage{multicol} % Style double colonne
\usepackage{abstract} % Customization de l'abstract
\usepackage{fancyhdr} % en-têtes et pieds de page
\usepackage{float} % Nécessaire pour les tables et figures dans l'environnement double colonne
\usepackage{array}

\usepackage[colorlinks=true,linkcolor=red,urlcolor=blue,filecolor=green]{hyperref} % hyperliens

% En-têtes et pieds de page
\pagestyle{fancy}
\fancyhead{} % Blank out the default header
\fancyfoot{} % Blank out the default footer
\fancyhead[C]{SY09 - TP2 \hfill Aude COLLEVILLE - Alexis MATHEY } % Custom header text
\fancyfoot[RO,LE]{\thepage} % Custom footer text

%\setlength{\parskip}{1ex} % espace entre paragraphes

\newcommand{\bsx}{\boldsymbol{x}}
\newcommand{\transp}{^{\mathrm{t}}}

\newcolumntype{C}[1]{>{\centering\arraybackslash}m{#1}}


%----------------------------------------------------------------------------------------

\title{Classification automatique}

\author{Alexis Mathey \& Aude Colleville}
\date{\today}

%----------------------------------------------------------------------------------------

\begin{document}

\maketitle % Insert title

\thispagestyle{fancy} % All pages have headers and footers


%----------------------------------------------------------------------------------------


\section{Visualisation des données}
Dans cet exercice, nous nous sommes intéressés à la visualisation de données à l'aide de l'analyse en composantes principales (ACP) ou de l'analyse factorielle d'un tableau de distance (AFTD) que l'on représente au moyen d'un diagramme de Shepard.

\subsection{Données \texttt{Iris}}\label{sub:visualisation_iris}
Les données \texttt{Iris} n'étant pas représentées sous forme d'un tableau de dissimilarités, on applique simplement l'ACP pour obtenir une représentation des variables quantitatives. On  peut remarquer sur la figure \ref{fig:acp_iris_1} que les données sont réparties en deux clusters. Cependant dès lors que l'on ajoute l'information de l'espèce à notre représentation, on remarque que l'on a en réalité trois clusters, cependant deux d'entre eux sont très proches (voir figure \ref{fig:acp_iris_2}).

Si l'on recherche une partition, nous pourrions nous attendre à la construction de 3 groupes correspondants aux espèces avec quelques erreurs. En effet, un algorithme de partitionnement devrait diviser le nuage de droite en deux clusters car ses extrémités sont bien distinctes mais des erreurs devraient forcément apparaître au milieu du nuage.
\begin{figure}[H]
	\centering
	\parbox{7cm}{
		\includegraphics[width=6cm]{img/acp_iris_1.png}
		\caption{Représentation des données \texttt{Iris} dans le premier plan factoriel de l'ACP.}
		\label{fig:acp_iris_1}}
	\qquad
	\begin{minipage}{7cm}
		\includegraphics[width=6cm]{img/acp_iris_2.png}
		\caption{Représentation des données \texttt{Iris} dans le premier plan factoriel de l'ACP colorées en fonction de l'espèce.}
		\label{fig:acp_iris_2}
	\end{minipage}
\end{figure}


\subsection{\label{sub:donnees_crabs}Données \texttt{Crabs}}
De même que pour les données \texttt{Iris}, les données \texttt{Crabs} n'étant pas représentées sous forme d'un tableau de dissimilarités, nous utilisons l'ACP afin d'obtenir une représentation des données dans le premier plan factoriel.
\begin{figure}[H]
	\centering
	\parbox{7cm}{
		\includegraphics[width=6cm]{img/acp_crabs_1.png}
		\caption{Représentation des données \texttt{Crabs} dans le premier plan factoriel de l'ACP.}
		\label{fig:acp_crabs_1}}
	\qquad
	\begin{minipage}{7cm}
		\includegraphics[width=6cm]{img/acp_crabs_2.png}
		\caption{Représentation des données \texttt{Crabs} dans le premier plan factoriel de l'ACP colorées en fonction du sexe.}
		\label{fig:acp_crabs_2}
	\end{minipage}
	\begin{minipage}{7cm}
		\includegraphics[width=6cm]{img/acp_crabs_3.png}
		\caption{Représentation des données \texttt{Crabs} dans le premier plan factoriel de l'ACP colorées en fonction de l'espèce.}
		\label{fig:acp_crabs_3}
	\end{minipage}
\end{figure}

La figure \ref{fig:acp_crabs_1} nous permet de distinguer deux clusters dans le jeu de données \texttt{Crabs}. Cependant par la suite, la figure \ref{fig:acp_crabs_2} nous permet de remarquer que l'information du sexe sépare également le jeu de données en deux clusters verticaux. On peut affirmer que le sexe est donc déterminé par la deuxième composante principale. Enfin la figure \ref{fig:acp_crabs_3} nous permet de retrouver nos deux clusters initiaux qui correspondent à la séparation des deux espèces. Ainsi l'espèce, à l'inverse, est déterminée par la première composante principale.

\subsection{\label{sub:donnees_mutations}Données \texttt{Mutations}}
Contrairement aux deux jeux de données précédents, il est possible de déclarer les données \texttt{Mutations} comme tableau de dissimilarités et ainsi d'appliquer l'AFTD à ce tableau.
\begin{figure}[H]
	\begin{center}
		\includegraphics[width=8cm]{img/aftd_mut.png}
		\caption{Représentation des données \texttt{Mutations} dans le premier plan factoriel de l'AFTD en $d=2$ variables}
		\label{fig:aftd_mut}
	\end{center}
\end{figure}
La figure \ref{fig:aftd_mut} nous permet de remarquer 3 clusters dans les données \texttt{Mutations}. Afin de connaître la qualité $Q$ de la représentation on applique la formule:
\begin{equation}
	Q = 100*\dfrac{\lambda_{1}+\lambda_{2}}{\sum_{i} (\lambda_{i})}
\end{equation}
Ainsi, on obtient que la qualité de la représentation est de $70,6\%$.

Par ailleurs, on peut également afficher le diagramme de Shepard afin d'attester de la qualité de la représentation.
\begin{figure}[H]
	\centering
	\parbox{7cm}{
		\includegraphics[width=6cm]{img/shepard_mut_2.png}
		\caption{Diagramme de Shepard sur les données \texttt{Mutations} pour 2 variables.}
		\label{fig:shepard_mut_2}}
	\qquad
	\begin{minipage}{7cm}
		\includegraphics[width=6cm]{img/shepard_mut_5.png}
		\caption{Diagramme de Shepard sur les données \texttt{Mutations} pour 5 variables.}
		\label{fig:shepard_mut_5}
	\end{minipage}
\end{figure}

Le diagramme de Shepard nous permet également d'avoir une représentation graphique de la qualité de l'AFTD. En effet, les distances initiales sont représentées par la diagonale bleue. Ainsi plus les distances s'éloignent de la diagonale, moins elles sont bonnes. La figure \ref{fig:shepard_mut_2} représente donc les distantes reconstruites avec une qualité de représentation de $70,6\%$ calculée pour 2 variables précédemment. La figure \ref{fig:shepard_mut_5} quant à elle représente le diagramme de Shepard pour l'AFTD avec 5 variables. On peut remarquer que les distances reconstruites sont bien plus proches de la diagonale que précédemment. En effet, la qualité de la représentation pour 5 variables est de 93,7\%. Ainsi, on peut affirmer que plus on augmente le nombre de variables, meilleure est la qualité de la représentation.

\section{Classification hiérarchique}
Dans cet exercice, on étudie les classifications hiérarchiques ascendantes et descendantes afin de pouvoir comparer nos résultats avec ceux obtenus précédemment.

\subsection{Données \texttt{Mutations}}
Dans la classification hiérarchique ascendante des données \texttt{Mutations} représentée ci-dessous (figure \ref{fig:cah_mut}), on remarque qu'il y a deux clusters évidents qui séparent les organismes unicellulaires des organismes multicellulaires. On peut également séparer les embranchements plus bas et repérer trois clusters qui séparent la moisissure des autres organismes unicellulaires. On peut ainsi de suite descendre de niveaux et augmenter le nombre de clusters progressivement.
\begin{figure}[H]
	\begin{center}
		\includegraphics[width=8cm]{img/cah_mut.png}
		\caption{Classification hiérarchique ascendante des données \texttt{Mutations}.}
		\label{fig:cah_mut}
	\end{center}
\end{figure}
On retrouve bien les résultats obtenus par l'AFTD dans la partie \ref{sub:donnees_mutations} puisque les points sont regroupés par proximité. On retrouve bien les trois clusters évidents du graphique de l'AFTD (figure \ref{fig:aftd_mut}).

\subsection{Données \texttt{Iris}}
Après un calcul des distances associées au jeu de données \texttt{Iris}, nous établissons sa classification hiérarchique ascendante (figure \ref{fig:cah_iris}). On retrouve bien sur cette hiérarchie les deux clusters principaux mis en évidence par l'ACP (Figure \ref{fig:acp_iris_1}) : l'un de ces clusters ne comporte que les iris d'espèce setosa tandis que l'autre cluster est un peu moins précis dans la classification.

La clusterisation par classification hiérarchique ascendante semble donc donner des résultats auxquels nous nous attendions et que nous avions évoqués dans la partie \ref{sub:visualisation_iris}.\\

À titre de comparaison, nous avons également établi la classification hiérarchique descendante des données \texttt{Iris} (figure \ref{fig:cdh_iris}).

Ces deux représentations donnent des classifications assez similaires. Les différences notables sont néanmoins :
\begin{itemize}
  \item Des hauteurs de clusters significativement plus élevées pour la classification ascendante,
  \item Une meilleure précision de la classification ascendante pour le classement de l'espèce setosa
\end{itemize}
Finalement, il faut également noter que l'algorithme de classification descendante est moins performant que celui de la classification ascendante.

\begin{figure}[H]
	\begin{center}
		\includegraphics[width=12cm]{img/cah_iris_species.png}
		\caption{Classification hiérarchique ascendante des données \texttt{Iris}.}
		\label{fig:cah_iris}
	\end{center}
\end{figure}

\begin{figure}[H]
	\begin{center}
		\includegraphics[width=12cm]{img/cdh_iris_species.png}
		\caption{Classification hiérarchique descendante des données \texttt{Iris}.}
		\label{fig:cdh_iris}
	\end{center}
\end{figure}


\section{Méthode des centres mobiles}
Dans cette partie, nous étudions les performances de la méthode des centres mobiles (aussi appelée "k-means") sur les trois jeux de données : \texttt{Iris}, \texttt{Crabs} et \texttt{Mutations}.
\subsection{Données \texttt{Iris}}
Nous avons appliqué l'algorithme "k-means" aux données \texttt{Iris} en faisant varier le nombre de clusters de deux à quatre (figures \ref{fig:kmeans_iris_2} à \ref{fig:kmeans_iris_4}). Les groupements obtenus ne sont pas surprenants au vu des observations faites dans la partie \ref{sub:visualisation_iris} et des résultats de la classification hiérarchique (figures \ref{fig:cah_iris} et \ref{fig:cdh_iris}).
\begin{figure}[H]
	\centering
	\parbox{7cm}{
		\includegraphics[width=6cm]{img/kmeans_iris_2.png}
		\caption{Partition des données \texttt{Iris} en 2 classes par la fonction k-means}
		\label{fig:kmeans_iris_2}}
	\qquad
	\begin{minipage}{5cm}
		\includegraphics[width=6cm]{img/kmeans_iris_3.png}
		\caption{Partition des données \texttt{Iris} en 3 classes par la fonction k-means}
		\label{fig:kmeans_iris_3}
	\end{minipage}
	\begin{minipage}{5cm}
		\includegraphics[width=6cm]{img/kmeans_iris_4.png}
		\caption{Partition des données \texttt{Iris} en 4 classes par la fonction k-means}
		\label{fig:kmeans_iris_4}
	\end{minipage}
\end{figure}

Afin d'étudier la stabilité du résultat de la partition en 3 classes par la méthode des centres mobiles, nous avons appliqué l'algorithme à nos données à plusieurs reprises. Cela a permis de mettre en évidence que l'algorithme "k-means" n'est pas totalement stable puisqu'il peut donner deux classifications différentes en 3 classes.

\begin{table}[H]
	\begin{center}
		\begin{tabular}{C{4cm} C{4cm} C{4cm}}
			Nombre d'éléments dans chaque cluster & Inertie intra-classe totale & $\frac{\mbox{Inertie inter-classes}}{\mbox{Inertie intra-classes}}$ \\
			\hline
			62, 50, 38 & 78.8 & 88.4\% \\
			96, 33 21 & 142.8 & 79\% 
		\end{tabular}
	\end{center}
\end{table}

La première ligne de ce tableau représente une partition de meilleure qualité que la seconde car l'inertie intra-classe est inférieure, ce qui signifie que chaque partie est plus "dense".\\

Afin de déterminer le nombre de classes optimal pour la partition, nous représentons la variation de l'inertie intra-classe minimale en fonction du nombre de clusters (figure \ref{fig:var_inertie_kmeans}). Grâce au coude de cette courbe, nous déterminons que le nombre de clusters optimal est de 3.

Ce nombre correspond bien au partitionnement réel du jeu de données puisque nous avons trois espèces.

\begin{figure}[H]
	\begin{center}
		\includegraphics[width=9cm]{img/var_inertie_kmeans.png}
		\caption{Variation de l'inertie intra-classe minimale en fonction du nombre de clusters}
		\label{fig:var_inertie_kmeans}
	\end{center}
\end{figure}


\subsection{Données \texttt{Crabs}}
Nous appliquons la méthode des "k-means" au jeu de données \texttt{Crabs} afin de le partitionner en 2 classes. En l'appliquant à répétition, nous obtenons deux classifications différentes : une qui sépare les données selon le sexe (figure \ref{fig:kmeans_crabs_sexe}) et une qui sépare les données selon l'espèce (figure \ref{fig:kmeans_crabs_espece}).

\begin{figure}[H]
	\centering
	\parbox{7cm}{
		\includegraphics[width=7cm]{img/crabs_kmeans_sexe.png}
		\caption{Première classification des données \texttt{Crabs} en 2 classes par la méthode des k-means}
		\label{fig:kmeans_crabs_sexe}}
	\qquad
	\begin{minipage}{7cm}
		\includegraphics[width=7cm]{img/crabs_kmeans_espece.png}
		\caption{Seconde classification des données \texttt{Crabs} en 2 classes par la méthode des k-means}
		\label{fig:kmeans_crabs_espece}
	\end{minipage}
\end{figure}

Nous effectuons ensuite une partition  des données en 4 classes (figure \ref{fig:crabs_kmeans_4}).On remarque que cette partition est une combinaison des deux classifications en 2 classes. Chaque classe semble donc représenter une combinaison sexe-espèce différente :
%%\begin{itemize}
%%	\item en haut à gauche, les crabes O féminins,
%%	\item en haut à droite, les crabes B féminins,
%%	\item en bas à gauche, les crabes O masculins,
%%	\item en bas à droite, les crabes B masculins.
%%\end{itemize}
%%La méthode des centres mobiles semble donc très efficace pour le partitionnement de ce jeu de données.

\begin{figure}[H]
	\begin{center}
		\includegraphics[width=8cm]{img/crabs_kmeans_sexe_espece.png}
		\caption{Partition des données \texttt{Crabs} en 4 classes par la méthode des k-means}
		\label{fig:crabs_kmeans_4}
	\end{center}
\end{figure}

\subsection{Données \texttt{Mutations}}
Dans cette partie, nous nous intéressons à nouveau au jeu de données \texttt{Mutations}. Nous avons d'abord calculé une représentation des données dans un espace de dimension $d=5$ à l'aide de l'AFTD. Nous appliquons ensuite la méthode des centres mobiles à cette représentation afin de la partitionner en 3 classes. 

La fonction \texttt{kmeans} donne 6 classifications différentes (dont deux sont représentées en figures \ref{fig:mut_kmeans_2} et \ref{fig:mu_kmeans_4}). Il est alors possible de déterminer la meilleure partition en comparant les inerties intra-classe totales de chacune (Table \ref{tab:inerties_mutations_3}). Le meilleure partition est donc la n\degres2 (représentée dans le premier plan factoriel de l'AFTD en figure \ref{fig:mut_kmeans_2}).

Cette partition correspond à la partition en 3 classes obtenue par la classification hiérarchique ascendante (figure \ref{fig:cah_mut}).

\begin{figure}[H]
	\centering
	\parbox{7cm}{
		\includegraphics[width=7cm]{img/mut_kmeans_2.png}
		\caption{Une des 6 classifications des données \texttt{Mutations} en 3 classes par la méthode k-means}
		\label{fig:mut_kmeans_2}}
	\qquad
	\begin{minipage}{7cm}
		\includegraphics[width=7cm]{img/mut_kmeans_4.png}
		\caption{Une des 6 classifications des données \texttt{Mutations} en 3 classes par la méthode k-means}
		\label{fig:mu_kmeans_4}
	\end{minipage}
\end{figure}

\begin{table}[H]
	\begin{center}
		\begin{tabular}{C{4cm} C{4cm}}
			Partition n\degres & Inertie intra-classe totale \\
			\hline
			1 & 4398,2 \\
			2 & 3621,9 \\
			3 & 4918,1 \\
			4 & 5092,5 \\
			5 & 4975 \\
			6 & 5237,2 \\
		\end{tabular}
	\end{center}
	\caption{Inerties intra-classe totales des différentes partitions en 3 classes possibles pour les données \texttt{Mutations}}
	\label{tab:inerties_mutations_3}
\end{table}




\section{Conclusion}
Ce TP nous a permis de découvrir et visualiser les résultats de différentes méthodes de classification automatique de données : les classifications hiérarchiques (ascendante et descendante) et la méthode des centres mobiles. Grâce aux visualisations, nous avons pu observer que ces méthodes permettent d'obtenir des partitionnements de données relativement pertinents en des temps d'exécution limités.

Néanmoins, il existe des cas où, de par la sélection aléatoire du point de départ, la méthode des centres mobiles donnera un grand nombre de partitions différentes. Il faudra alors s'aider de quelques indicateurs numériques (inertie intra-classe totale, taille des classes, etc) et de sa connaissance du jeu de données afin de déterminer quelle partition est la plus représentative des données.


%----------------------------------------------------------------------------------------


\end{document}
